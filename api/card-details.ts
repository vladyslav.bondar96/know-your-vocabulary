import { appApiClient } from './app-api-client';
import { Word } from "@/types";

const word_url = '/api/word';

export const addNewWord = async (requestBody: { word: Word } ) => {
    const response = await appApiClient.post(word_url, requestBody);
    
    return response.json();
}

export const updateWord = async (requestBody: Word) => {
    const response = await appApiClient.put(word_url, requestBody);

    return response.json();
}

export const removeWord = async (wordId: string): Promise<{ isRemoved: boolean, wordId: string }> => {
    const response  = await appApiClient.delete(`${word_url}/${wordId}`,);

    return { isRemoved: response.status === 204, wordId  };
}
