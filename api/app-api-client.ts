// Temperary soultion

/* 
    TODO: replace base URL and all app varables to env file
*/

export const BASE_URL = 'http://localhost:3000';

class AppApiClient {
    private headers = {
        'Content-Type': 'application/json',
    }

    constructor(readonly baseUrl: string) {}

    post<T>(url: string, requestBody: T) {
        return fetch(url, {  
            method: 'POST',
            headers: this.headers,
            body: JSON.stringify(requestBody)
        })
    }

    put<T>(url: string, requestBody: T) {
        return fetch(url, {  
            method: 'PUT',
            headers: this.headers,
            body: JSON.stringify(requestBody)
        })
    }

    delete<T>(url: string) {
        return fetch(url, {  
            method: 'DELETE',
            headers: this.headers,
        })
    }
}

const appApiClient = new AppApiClient(BASE_URL);

export { appApiClient }
