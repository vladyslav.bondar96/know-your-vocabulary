import Head from "next/head";
import { AppProps } from "next/app";
import { QueryClient, QueryClientProvider } from "react-query";

import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.css";
import "tailwindcss/tailwind.css";

const queryClient = new QueryClient();

export default function App({ Component, pageProps }: AppProps) {
  return (
    <QueryClientProvider client={queryClient}>
      <Head>
        <title>Words Dictation</title>
      </Head>
      <Component {...pageProps} />
    </QueryClientProvider>
  );
}
