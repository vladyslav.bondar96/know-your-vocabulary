import type { NextApiRequest, NextApiResponse } from 'next'
import Cors from 'cors'

import { wordStore } from '../../../lib/WordController';

// const cors = Cors({
//   methods: ['POST', 'GET', 'PUT', 'DELETE'],
// })

// function runMiddleware(
//   req: NextApiRequest,
//   res: NextApiResponse,
//   fn: Function
// ) {
//   return new Promise((resolve, reject) => {
//     fn(req, res, (result: any) => {
//       if (result instanceof Error) {
//         return reject(result)
//       }

//       return resolve(result)
//     })
//   })
// }

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {

  switch(req.method) {
    case 'POST':
     const { word } = req.body;

      wordStore.addWord(word);
    case 'PUT':
        const updatedWord = wordStore.updateWord(req.body);

        res.status(200).json(updatedWord)

        return;
  }

  res.status(404).json({ message: 'incorrect endpint path' });
}