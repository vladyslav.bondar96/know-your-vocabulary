import type { NextApiRequest, NextApiResponse } from 'next'

import { wordStore } from '../../../lib/WordController';


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  switch(req.method) {
    case 'DELETE':
        const { query } = req
        const { id } = query
        wordStore.removeWord(id as string);
        
        res.status(204).end();

        return;
  }

  res.status(404).json({ message: 'incorrect endpint path' });
}