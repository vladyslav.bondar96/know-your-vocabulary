import { GetStaticPaths, GetStaticProps } from 'next'
import { ParsedUrlQuery } from 'querystring'

import { wordStore } from '../../lib/WordController';
import { getCardById, getCardIds } from '../../lib/card';
import { Word } from '../../types/';
import { CardDictation } from '../../components/CardDictation';

interface CardProps {
    words: Word[];
}

export default function Card({ words }: CardProps) {
    return <CardDictation words={words}/>;
}

interface IParams extends ParsedUrlQuery {
    id: string
}

export const  getStaticPaths: GetStaticPaths = () => {
    const paths = getCardIds();

    return {
        paths,
        fallback: false,
    };
}

export const getStaticProps: GetStaticProps = (context) => {
    const { id } = context.params as IParams
    const words = getCardById(id);

    return {
        props: {
            words,
        }
    };
}

