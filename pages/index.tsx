import { wordStore } from "../lib/WordController";
import { CardPreview } from "../components/CardPreview";

// TODO: replace this to file for common app types
// investigate where these types should be located in your project
interface Card {
  id: number;
  name: string;
}

interface HomeProps {
  wordCards: Card[];
}

export default function Home({ wordCards }: HomeProps) {
  return (
    <div className="container grid grid-rows-3 grid-flow-col gap-4 my-10">
      {wordCards.map((wordcard) => (
        <CardPreview {...wordcard} />
      ))}
    </div>
  );
}

export function getStaticProps() {
  const wordCards = wordStore.getCards();

  return {
    props: {
      wordCards,
    },
  };
}
