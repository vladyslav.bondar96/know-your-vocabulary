import { FC } from "react";

export const QuizCard: FC<{ translation: string[] }> = ({ translation }) => {
  return (
    <div className="flex flex-col py-4 px-4 mb-4 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <h1 className="text-2xl text-center cursor-pointer">
        {translation.join(", ")}
      </h1>
    </div>
  );
};
