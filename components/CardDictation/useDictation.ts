import { useState, useCallback, useEffect, useMemo } from 'react';
import { shuffleArray } from '../../utils'
import { Word } from './index';

export interface DictationWord extends Word {
    isCorrect: boolean,
    userAnswer: string,
}

interface UseDictationOutput {
    isFinished: boolean,
    currentDictatedWord: Word,
    answerTranslation(answer: string): void,
    skipWord(): void,
    dictation: DictationWord[],
}

export const useDictation = (words: Word[]): UseDictationOutput => {
    const dictationsWords = /* shuffleArray(words) */ words.map((word) => ({...word, isCorrect: false, userAnswer: "" }))

    const [isFinished, setIsFinished] = useState<boolean>(true);
    const [dictation, setDictation] = useState<DictationWord[]>(dictationsWords);
    const [dictatedWordIndex, setDictatedWordIndex] = useState<number>(0);

    const currentDictatedWord = dictation[dictatedWordIndex];

    const setNextDictatedWordIndex = useCallback(() => {
        const nextIndex = dictatedWordIndex + 1;

        if (nextIndex >= dictation.length -1) {
            setIsFinished(true);

            return;
        }

        setDictatedWordIndex(nextIndex);
    }, [dictatedWordIndex, dictation])

    const answerTranslation = useCallback((answer: string) => {
        if (isFinished) return;

        dictation[dictatedWordIndex].isCorrect = 
            dictation[dictatedWordIndex].word.trim() === answer.trim();
        dictation[dictatedWordIndex].userAnswer = answer;
    
        setDictation([...dictation]);
        
        setNextDictatedWordIndex();
    }, [dictatedWordIndex, dictation]);

    const skipWord = () => {
        if (isFinished) return;

        setNextDictatedWordIndex();
    }

    return {
        isFinished,
        currentDictatedWord,
        answerTranslation,
        skipWord,
        dictation, 
    }
}