import { FC } from "react";
import Link from "next/link";
import { DictationWord } from "./useDictation";
import { CenterPageContainer } from "@/commonComponents/CenterPageContainer";
import { DictationResultTable } from "./DictationResultTable";

interface DictationResultProps {
  dictationResult: DictationWord[];
  onTryAgain(): void;
}

export const DictationResult: FC<DictationResultProps> = ({
  dictationResult,
}) => {
  return (
    <CenterPageContainer>
      <div className="flex flex-col h-3/4">
        <DictationResultTable dictationResult={dictationResult} />
        <div className="mt-4 flex justify-end">
          <button
            type="button"
            className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mb-2 mr-2"
          >
            Try Again
          </button>
          <Link
            href="/"
            className="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mb-2"
          >
            Home
            {/* <button type="button">Quit</button> */}
          </Link>
        </div>
      </div>
    </CenterPageContainer>
  );
};
