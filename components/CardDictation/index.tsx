import { FC } from "react";
import { useDictation } from "./useDictation";
import { AnswerInput } from "./AnswerInput";
import { CenterPageContainer } from "../../commonComponents/CenterPageContainer";
import { QuizCard } from "./QuizCard";
import { DictationResult } from "./DictationResult";

export type Word = {
  word: string;
  tranlations: string[];
  date: string;
};

interface CardProps {
  words: Word[];
}

export const CardDictation: FC<CardProps> = ({ words }) => {
  const {
    isFinished,
    currentDictatedWord,
    answerTranslation,
    skipWord,
    dictation,
  } = useDictation(words);

  if (isFinished) {
    return (
      <DictationResult
        dictationResult={dictation}
        onTryAgain={() => console.log("loh suqo")}
      />
    );
  }

  return (
    <CenterPageContainer>
      <div className="flex flex-col w-4/12">
        <QuizCard translation={currentDictatedWord?.tranlations} />
        <AnswerInput onSubmit={answerTranslation} onSkip={skipWord} />
      </div>
    </CenterPageContainer>
  );
};
