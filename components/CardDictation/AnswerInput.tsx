import { useEffect, useRef, FC, KeyboardEvent, useCallback } from "react";

interface AnswerInputProps {
  onSubmit(word: string): void;
  onSkip(): void;
}

export const AnswerInput: FC<AnswerInputProps> = ({ onSubmit, onSkip }) => {
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (!inputRef.current) return;

    inputRef.current.focus();
  }, [inputRef]);

  const handleSubmitAnswer = useCallback(() => {
    if (!inputRef.current) return;
    onSubmit(inputRef.current.value);
    inputRef.current.value = "";
  }, [inputRef, onSubmit]);

  const handleEnter = useCallback(
    (event: KeyboardEvent<HTMLInputElement>) => {
      if (event.key === "Enter" && inputRef.current) {
        onSubmit(inputRef.current.value);
        inputRef.current.value = "";
      }
    },
    [inputRef]
  );

  return (
    <>
      <div className="w-full mb-4 flex flex-col py-4 px-4 relative bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
        <input
          onKeyPress={handleEnter}
          ref={inputRef}
          type="text"
          className="text-center text-2xl block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        />
      </div>
      <div className="flex justify-end">
        <button
          onClick={onSkip}
          type="button"
          className="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm px-3 py-2 text-center mr-3"
        >
          I don't remeber
        </button>
        <button
          onClick={handleSubmitAnswer}
          type="button"
          className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 font-medium rounded-lg text-sm px-3 py-2 text-center"
        >
          Answer
        </button>
      </div>
    </>
  );
};
