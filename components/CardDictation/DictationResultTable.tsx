import { FC } from "react";
import classnames from "classnames";
import { DictationWord } from "./useDictation";
import CheckCircle from "@/public/icons/check-circle.svg";
import CloseCircle from "@/public/icons/close-circle.svg";

interface DictationResultTableProps {
  dictationResult: DictationWord[];
}

export const DictationResultTable: FC<DictationResultTableProps> = ({
  dictationResult = [],
}) => {
  return (
    <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr className="sticky top-0 bg-gray-50">
            <th scope="col" className="px-6 py-3"></th>
            <th scope="col" className="px-6 py-3">
              Word
            </th>
            <th scope="col" className="px-6 py-3">
              Translations
            </th>
            <th scope="col" className="px-6 py-3">
              Your answer
            </th>
          </tr>
        </thead>
        <tbody>
          {dictationResult.map((dictationResultWord) => (
            <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
              <th
                scope="row"
                className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
              >
                {dictationResultWord.isCorrect ? (
                  <CheckCircle className="w-5 h-5 text-green-600" />
                ) : (
                  <CloseCircle className="w-5 h-5 text-red-600" />
                )}
              </th>
              <th
                scope="row"
                className={classnames(
                  "px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white",
                  dictationResultWord.isCorrect
                    ? "text-green-600"
                    : "text-red-600"
                )}
              >
                {dictationResultWord.word}
              </th>
              <td className="px-6 py-4">
                {dictationResultWord.tranlations.join(", ")}
              </td>
              <td className="px-6 py-4">{dictationResultWord.userAnswer}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
