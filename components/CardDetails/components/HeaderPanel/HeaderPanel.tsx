import { FC, useCallback } from "react";
import { IconButton } from "@/commonComponents/Button";

import Plus from "@/public/icons/plus.svg";
import TrashBin from "@/public/icons/trash_bin.svg";

import {
  useCardState,
  useCardDispatch,
  CardActionTypes,
} from "../../context/CardContext";
import { Word } from "@/types";

export const HeaderPanel: FC = () => {
  const { words } = useCardState();
  const cardDispatch = useCardDispatch();

  const newWordTemplate: Word = {
    id: "",
    word: "",
    tranlations: [],
    date: new Date().toString(),
    cardId: words[0].cardId,
    correctAnwers: 0,
    note: "",
  };

  const handleAddNewWord = useCallback(() => {
    cardDispatch({
      type: CardActionTypes.addNewWord,
      payload: { rowForAdding: newWordTemplate },
    });
  }, [cardDispatch]);

  return (
    <div className="container flex grow-0 items-center mb-5">
      <div className="w-full flex flex-row justify-between">
        <div>
          <h1>Poker face</h1>
        </div>
        <div>
          <IconButton
            className="mr-2"
            onClick={handleAddNewWord}
            icon={<Plus className="w-4 h-4" />}
          />
          <IconButton color="red" icon={<TrashBin className="w-4 h-4" />} />
        </div>
      </div>
    </div>
  );
};
