import { FC, useCallback } from "react";
import { CardRowEditItem } from "./CardRowEditItem";
import { Word } from "@/types";
import { useMutation } from "react-query";
import { addNewWord } from "@/api/card-details";
import {
  useCardState,
  useCardDispatch,
  CardActionTypes,
} from "../../context/CardContext";

interface AddCardRowItemProps {
  word: Word;
}

export const AddCardRowItem: FC<AddCardRowItemProps> = ({ word }) => {
  const cardDispatch = useCardDispatch();
  const { words } = useCardState();

  const cardId = words[0].cardId;

  const addMutation = useMutation(addNewWord, {
    onSuccess: (data) => {
      const { word } = data;
      cardDispatch({
        type: CardActionTypes.saveNewWord,
        payload: { word },
      });
    },
  });

  const handleSave = useCallback(
    (word: Word) => {
      addMutation.mutate({ word });
    },
    [addMutation, cardId]
  );

  return <CardRowEditItem word={word} onSave={handleSave} />;
};
