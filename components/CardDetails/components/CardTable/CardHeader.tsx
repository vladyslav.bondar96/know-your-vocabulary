export const CardHeader = () => {
  return (
    <thead className="text-xs text-gray-700 uppercase">
      <tr className="sticky top-0 bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
        <th scope="col" className="px-6 py-3">
          Word
        </th>
        <th scope="col" className="px-6 py-3">
          Translations
        </th>
        <th scope="col" className="px-6 py-3">
          Note
        </th>
        <th scope="col" className="px-6 py-3">
          Correct Answers
        </th>
        <th scope="col" className="px-6 py-3">
          Learnt
        </th>
        <th scope="col" className="px-6 py-3">
          Action
        </th>
      </tr>
    </thead>
  );
};
