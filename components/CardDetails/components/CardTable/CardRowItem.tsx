import { FC, useCallback } from "react";
import { Word } from "../../../../types";

import { IconButton } from "@/commonComponents/Button";
import Edit from "@/public/icons/edit.svg";
import TrashBin from "@/public/icons/trash_bin.svg";

interface CardRowItemProps {
  wordItem: Word;
  onEdit(wordId: string): void;
  onRemove(wordId: string): void;
}

export const CardRowItem: FC<CardRowItemProps> = ({
  wordItem,
  onEdit,
  onRemove,
}) => {
  const { id, word, tranlations, note, correctAnwers } = wordItem;

  const handleEdit = useCallback(() => {
    onEdit(id);
  }, [id, onEdit]);

  const handleRemove = useCallback(() => {
    onRemove(id);
  }, [id, onRemove]);

  return (
    <tr
      style={{ height: "65px" }}
      className="h-100 bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600"
    >
      <th
        scope="row"
        className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
      >
        {word}
      </th>
      <td className="px-6 py-4">{tranlations.join(", ")}</td>
      <td className="px-6 py-4">{note}</td>
      <td className="px-6 py-4">{correctAnwers}</td>
      <td className="px-6 py-4">{0}</td>
      <td className="px-6 py-4 flex flex-row">
        <IconButton
          className="mr-2"
          icon={<Edit className="w-3 h-3" />}
          onClick={handleEdit}
        />
        <IconButton
          color="red"
          icon={false ? "L" : <TrashBin className="w-3 h-3" />}
          onClick={handleRemove}
        />
      </td>
    </tr>
  );
};
