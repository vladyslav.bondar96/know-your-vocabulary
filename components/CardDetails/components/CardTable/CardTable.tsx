import { FC, useCallback, useEffect, useRef } from "react";
import { CardHeader } from "./CardHeader";
import {
  CardActionTypes,
  useCardDispatch,
  useCardState,
} from "../../context/CardContext";
import { AddCardRowItem } from "./AddCardRowItem";
import { CardRowEditItem } from "./CardRowEditItem";
import { CardRowItem } from "./CardRowItem";
import { useMutation } from "react-query";
import { updateWord, removeWord } from "@/api/card-details";
import { Word } from "@/types";

export const CardTable: FC = () => {
  const cardDispatch = useCardDispatch();
  const tableRef = useRef<HTMLDivElement | null>(null);
  const { words, editableRowId, rowForAdding } = useCardState();

  useEffect(() => {
    if (rowForAdding) {
      tableRef.current?.scroll(0, 0);
    }
  }, [rowForAdding, tableRef]);

  const updateWordMutations = useMutation(updateWord, {
    onSuccess: (data) => {
      cardDispatch({
        type: CardActionTypes.updatedWord,
        payload: { word: data },
      });
    },
  });

  const handleDispatchUpdates = useCallback(
    (wordId: string) => {
      return () => {
        cardDispatch({
          type: CardActionTypes.setEditableRowId,
          payload: { editableRowId: wordId },
        });
      };
    },
    [cardDispatch]
  );

  const handleSaveUpdates = useCallback(
    (word: Word) => {
      updateWordMutations.mutate(word);
    },
    [updateWordMutations]
  );

  const removeMutation = useMutation(removeWord, {
    onSuccess: (data) => {
      const { isRemoved, wordId } = data;

      if (!isRemoved) return;

      cardDispatch({
        type: CardActionTypes.removeWord,
        payload: { id: wordId },
      });
    },
  });

  const handleRemoveWord = useCallback(
    (wordId: string) => {
      return () => {
        removeMutation.mutate(wordId);
      };
    },
    [removeMutation]
  );

  return (
    <div
      ref={tableRef}
      className="container flex-grow overflow-auto  rounded-lg drop-shadow-xl"
    >
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 shadow-md">
        <CardHeader />
        <tbody>
          {rowForAdding && (
            <AddCardRowItem key={rowForAdding.id} word={rowForAdding} />
          )}
          {words.map((word) => {
            return word.id === editableRowId ? (
              <CardRowEditItem
                key={word.id}
                word={word}
                onSave={handleSaveUpdates}
              />
            ) : (
              <CardRowItem
                key={word.id}
                wordItem={word}
                onEdit={handleDispatchUpdates(word.id)}
                onRemove={handleRemoveWord(word.id)}
              />
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
