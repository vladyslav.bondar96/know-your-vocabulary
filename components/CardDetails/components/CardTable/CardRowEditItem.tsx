import { ChangeEvent, FC, useCallback, useState, useMemo } from "react";
import { Word } from "../../../../types";
import { IconButton } from "@/commonComponents/Button";
import Check from "@/public/icons/check.svg";
import Close from "@/public/icons/close.svg";

interface CardRowEditItemProps {
  word: Word;
  onSave(word: Word): void;
}

export const CardRowEditItem: FC<CardRowEditItemProps> = ({ word, onSave }) => {
  const [editableWord, setEditableWord] = useState<Word>(word);

  const changeWordByPropName = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      const { name, value } = event.target;
      let updatedValue: string | string[] = value;

      if (name === "tranlations") {
        updatedValue = value.split(",");
      }

      setEditableWord((prevEditableWord) => ({
        ...prevEditableWord,
        [name]: updatedValue,
      }));
    },
    [setEditableWord]
  );

  const handleSave = useCallback(() => {
    onSave(editableWord);
  }, [onSave, editableWord]);

  const handleCancel = useCallback(() => {
    setEditableWord(word);
  }, [word]);

  return (
    <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
      <th
        scope="row"
        className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
      >
        <input
          name="word"
          value={editableWord.word}
          onChange={changeWordByPropName}
          type="text"
          className="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 sm:text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        />
      </th>
      <td className="px-6 py-4">
        <input
          name="tranlations"
          value={editableWord.tranlations}
          onChange={changeWordByPropName}
          type="text"
          className="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 sm:text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        />
      </td>
      <td className="px-6 py-4">
        <input
          name="note"
          value={editableWord.note}
          onChange={changeWordByPropName}
          type="text"
          className="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 sm:text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        />
      </td>
      <td className="px-6 py-4">{editableWord.correctAnwers}</td>
      <td className="px-6 py-4">{0}</td>
      <td className="px-6 py-4">
        <IconButton
          color="green"
          className="mr-2"
          icon={false ? "L" : <Check className="w-3 h-3" />}
          onClick={handleSave}
        />
        <IconButton
          color="red"
          onClick={handleCancel}
          icon={<Close className="w-3 h-3" />}
        />
      </td>
    </tr>
  );
};
