import { FC } from "react";

import { Word } from "../../types/";
import { CardContextProvider } from "./context/CardContext";
import { CardTable } from "./components/CardTable";
import { HeaderPanel } from "./components/HeaderPanel";

interface CardDetailsProps {
  words: Word[];
}

export const CardDetails: FC<CardDetailsProps> = ({ words }) => {
  return (
    <CardContextProvider words={words}>
      <div className="container flex flex-col py-8">
        <HeaderPanel />
        <CardTable />
      </div>
    </CardContextProvider>
  );
};
