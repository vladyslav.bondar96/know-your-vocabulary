import  { Word }from "@/types";

export interface CardState {
  editableRowId: null | string;
  rowForAdding: null | Word;
  words: Word[];
}

export enum CardActionTypes {
  setEditableRowId = "setEditableRowId",
  setRowForEdition = "setRowForEdition",
  setWords = "setWords",
  addNewWord = "addNewWord",
  saveNewWord = "saveNewWord",
  updatedWord = "updatedWord",
  removeWord = "removeWord",
}

type AddNewWord = {
  type: CardActionTypes.addNewWord;
  payload: { rowForAdding: Word };
};

type SetEditableRowId = {
  type: CardActionTypes.setEditableRowId;
  payload: { editableRowId: string };
};

type SaveNewWord = {
  type: CardActionTypes.saveNewWord;
  payload: { word: Word };
};

type UpdateWord = {
  type: CardActionTypes.updatedWord;
  payload: { word: Word };
};

type RemoveWord = {
  type: CardActionTypes.removeWord;
  payload: { id: string };
};

type SetWords = {
  type: CardActionTypes.setWords;
  payload: { words: Word[] };
};

export type CardActions =
  | AddNewWord
  | SetEditableRowId
  | SaveNewWord
  | UpdateWord
  | RemoveWord
  | SetWords;