import { CardActions, CardActionTypes, CardState } from "./types";

export const initialCardDetailsState: CardState = {
  editableRowId: null,
  rowForAdding: null,
  words: [],
};

export const cardReducer = (
  cardState: CardState,
  action: CardActions
): CardState => {
  switch (action.type) {
    case CardActionTypes.setWords:
      return {
        ...cardState,
        words: action.payload.words,
      };
    case CardActionTypes.addNewWord: {
      return {
        ...cardState,
        rowForAdding: action.payload.rowForAdding,
        editableRowId: null,
      };
    }
    case CardActionTypes.setEditableRowId:
      return {
        ...cardState,
        editableRowId: action.payload.editableRowId,
      };
    case CardActionTypes.saveNewWord:
      return {
        ...cardState,
        rowForAdding: null,
        words: [action.payload.word, ...cardState.words],
      };
    case CardActionTypes.updatedWord:
      return {
        ...cardState,
        words: cardState.words.map((word) =>
          word.id === action.payload.word.id ? action.payload.word : word
        ),
        rowForAdding: null,
        editableRowId: null,
      };
    case CardActionTypes.removeWord:
      return {
        ...cardState,
        words: cardState.words.filter((word) => word.id !== action.payload.id),
      };
    default:
      throw Error("Unknown action: " + action);
  }
}
