import {
  FC,
  ReactNode,
  Dispatch,
  createContext,
  useReducer,
  useContext,
} from "react";

import { Word } from "@/types";
import { CardActions, CardState } from "./types";
import { initialCardDetailsState, cardReducer } from "./CardState";

const CardContext = createContext<CardState>(initialCardDetailsState);

const CardDispatchContext = createContext<Dispatch<CardActions> | null>(null);

interface CardProviderProps {
  words: Word[];
  children?: ReactNode;
}

export const CardContextProvider: FC<CardProviderProps> = ({
  words,
  children,
}) => {
  const [card, dispatch] = useReducer(cardReducer, {
    ...initialCardDetailsState,
    words,
  });

  return (
    <CardContext.Provider value={card}>
      <CardDispatchContext.Provider value={dispatch}>
        {children}
      </CardDispatchContext.Provider>
    </CardContext.Provider>
  );
};

export const useCardState = () => useContext(CardContext);

export const useCardDispatch = () => {
  const dispatch = useContext(CardDispatchContext);

  if (!dispatch) {
    throw new Error("dispatch does not exist");
  }

  return dispatch;
};
