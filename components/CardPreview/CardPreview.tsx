import { FC } from "react";
import Link from "next/link";

interface CardPreviewProps {
  id: number;
  name: string;
}

export const CardPreview: FC<CardPreviewProps> = ({ id, name }) => {
  return (
    <div className="flex flex-col items-center py-3 w-1/3 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <h5 className="mb-1 text-xl font-medium text-gray-900 hover:text-sky-600 hover:cursor-pointer">
        <Link href={`/details-card/${id}`}>{name}</Link>
      </h5>
      <span className="text-sm text-gray-500 dark:text-gray-400">
        completed - 0%
      </span>
      <div className="flex mt-4 space-x-3">
        <Link
          href={`/learn-card/${id}`}
          className="inline-flex items-center px-4 py-2 text-sm font-medium text-center text-gray-900 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-700 dark:focus:ring-gray-700"
        >
          Learn
        </Link>
        <Link
          href={`/dictation-card/${id}`}
          className="inline-flex items-center px-4 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Dictation
        </Link>
      </div>
    </div>
  );
};
