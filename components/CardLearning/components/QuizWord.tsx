import classnames from "classnames";
import { FC, useCallback, useState } from "react";
import { LearningWord } from "../useLearnWords";

interface QuizWordProps {
  word: LearningWord;
}

export const QuizWord: FC<QuizWordProps> = ({ word }) => {
  const [isWordBlured, setIsWordBlured] = useState<boolean>(true);

  const unblurWord = useCallback(() => {
    setIsWordBlured(false);

    setTimeout(() => {
      setIsWordBlured(true);
    }, 1000);
  }, []);

  return (
    <div className="flex flex-col py-4 px-4 mb-4 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <h1
        className={classnames("text-2xl pb-4 text-center cursor-pointer", {
          "blur-sm": isWordBlured,
        })}
        onClick={unblurWord}
      >
        {word.word}
      </h1>
      <hr />
      <div className="my-4">{word.tranlations.join(", ")}</div>
    </div>
  );
};
