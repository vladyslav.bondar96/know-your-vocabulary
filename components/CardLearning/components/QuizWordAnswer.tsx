import { FC, useMemo, useCallback, KeyboardEvent, useRef } from "react";
import classnames from "classnames";
import { LearningWord } from "../useLearnWords";

interface QuizWordAnswerProps {
  word: LearningWord;
  onPressEnter(ortography: string): void;
}

export const QuizWordAnswer: FC<QuizWordAnswerProps> = ({
  word,
  onPressEnter,
}) => {
  const REPEAT_TIMES = 7;
  const inputRef = useRef<HTMLInputElement | null>(null);

  const backgroundBlock = useMemo(() => {
    const blocks = [];

    while (blocks.length !== REPEAT_TIMES) {
      blocks.push(
        <div
          key={blocks.length}
          className={classnames(
            "flex-grow",
            word.ortography[blocks.length]
              ? word.ortography[blocks.length] === word.word
                ? "bg-green-500"
                : "bg-red-500"
              : ""
          )}
        />
      );
    }

    return blocks;
  }, [word.word, word.ortography]);

  const handleEnterWord = useCallback(
    (event: KeyboardEvent<HTMLInputElement>) => {
      if (event.key === "Enter" && inputRef.current) {
        onPressEnter(inputRef.current.value || "");
        inputRef.current.value = "";
      }
    },
    [inputRef]
  );

  return (
    <div className="w-full flex flex-col py-4 px-4 relative bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <div className="absolute top-0 left-0 right-0 bottom-0 z-0 flex flex-row">
        {backgroundBlock}
      </div>
      <div className="relative top-0 left-0 right-0 bottom-0 z-1">
        <input
          onKeyPress={handleEnterWord}
          ref={inputRef}
          type="text"
          className="z-1 text-center text-2xl block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        />
      </div>
    </div>
  );
};
