import { FC } from "react";

import { Word } from "../../types/";
import { CenterPageContainer } from "../../commonComponents/CenterPageContainer";
import { useLearnWords } from "./useLearnWords";

import { QuizWord } from "./components/QuizWord";
import { QuizWordAnswer } from "./components/QuizWordAnswer";

interface CardLearningProps {
  words: Word[];
}

export const CardLearning: FC<CardLearningProps> = ({ words }) => {
  const { currentQuizedWord, addWordOrtography, isFinished } =
    useLearnWords(words);

  if (isFinished) {
    return <>FINISHED!</>;
  }

  return (
    <CenterPageContainer>
      <div className="flex flex-col w-4/12">
        <QuizWord word={currentQuizedWord} />
        <QuizWordAnswer
          word={currentQuizedWord}
          onPressEnter={addWordOrtography}
        />
      </div>
    </CenterPageContainer>
  );
};
