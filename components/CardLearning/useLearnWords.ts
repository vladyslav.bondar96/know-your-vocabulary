import { useState, useCallback } from 'react';

import { Word } from '../../types/';
import { shuffleArray } from '../../utils'

export interface LearningWord extends Word {
    ortography: string[]
}

interface UseLearnWordsOutput {
    currentQuizedWord: LearningWord,
    addWordOrtography(wordOrtography: string): void,
    isFinished: boolean,
}

export const useLearnWords = (words: Word[]): UseLearnWordsOutput => {
    const initialLearningWords = shuffleArray(words).map(word => ({ ...word, ortography: [] }));

    const [learningWords, setLearningWords] = useState<LearningWord[]>(initialLearningWords)
    const [learningWordIndex, setLearningWordIndex] = useState<number>(0);

    const addWordOrtography = useCallback((wordOrtography: string) => {
        console.log(wordOrtography)
        
        learningWords[learningWordIndex].ortography = [
            ...learningWords[learningWordIndex].ortography,
            wordOrtography
        ]

        setLearningWords([...learningWords]);

        if (learningWords[learningWordIndex].ortography.length > 7) {
            // TODO: refactor
            if (learningWordIndex === learningWords.length - 1) return 
            setLearningWordIndex(learningWordIndex + 1);
        }

    }, [learningWordIndex, learningWords])

    return {
        currentQuizedWord: learningWords[learningWordIndex],
        addWordOrtography,
        isFinished: learningWordIndex === learningWords.length,
    }
}
