import { FC, ReactNode } from "react";

interface CenterPageContainerProps {
  children: ReactNode;
}

export const CenterPageContainer: FC<CenterPageContainerProps> = ({
  children,
}) => (
  <div className="flex h-screen w-full flex-col justify-center items-center">
    {children}
  </div>
);
