import { FC, ButtonHTMLAttributes, ReactNode } from "react";
import classnames from "classnames";

interface IconButton extends ButtonHTMLAttributes<HTMLButtonElement> {
  color?: "blue" | "red" | "green";
  icon: ReactNode | JSX.Element;
}

export const IconButton: FC<IconButton> = ({
  color = "blue",
  icon,
  className = "",
  ...restProps
}) => {
  const mapClsses = {
    red: "text-red-700 border-red-700 hover:bg-red-700 focus:ring-red-300 dark:border-red-500 dark:text-red-500 dark:hover:text-white dark:focus:ring-red-800 dark:hover:bg-red-500",
    blue: "text-blue-700 border-blue-700 hover:bg-blue-700 focus:ring-blue-300 dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:focus:ring-blue-800 dark:hover:bg-blue-500 ",
    green:
      "text-green-700 border-green-700 hover:bg-green-700 focus:ring-green-300 dark:border-green-500 dark:text-green-500 dark:hover:text-white dark:focus:ring-green-800 dark:hover:bg-green-500 ",
  };

  return (
    <button
      type="button"
      className={classnames(
        "border hover:text-white focus:ring-4 focus:outline-none rounded-full p-2 text-center inline-flex items-center text-sm",
        className,
        mapClsses[color]
      )}
      {...restProps}
    >
      {icon}
    </button>
  );
};
