import * as fs from 'fs';
import uniqid from 'uniqid';

interface Word {
    word: string;
    tranlations: string[],
    note?: string;
    date: string;
}

interface WordStoreItem extends Word {
    id: string;
    cardId: number;
    correctAnwers: number;
}


class WordList {
    private fullpath: string;

    constructor(readonly fileDir: string, readonly fileName: string) {
        this.fullpath = `${fileDir}${fileName}`;

        this.findOrCreateFile(this.fullpath);
    }

    private findOrCreateFile(fullpath: string) {
        if (!fs.existsSync(fullpath)) {
            try {
                fs.writeFileSync(fullpath, JSON.stringify([]));
            } catch (err) {
                console.error('An error occurred while creating the file:', err);
            }
        }
        
    }

    private get wordsStore(): WordStoreItem[] {
          const data = fs.readFileSync(this.fullpath, 'utf8');

        return data.length ? JSON.parse(data) : [];  
    }

    private set wordsStore(updatedWordStore: WordStoreItem[]) {
        fs.writeFileSync(this.fullpath, JSON.stringify(updatedWordStore), 'utf8');
    }

    // private createWordStoreItem(words: Word[], cardId: number): WordStoreItem[] {
    //     return words.map((word) => ({ ...word, id: uniqid(), cardId, correctAnwers: 0, note: '' }) )
    // }
    // createWordsCard(words: Word[]) {
    //     try {
    //         const wordsStore = this.getWordsStore();
    //         const newCardId = wordsStore[wordsStore.length - 1].cardId + 1;
    //         const wordsItem: WordStoreItem[] = this.createWordStoreItem(words, newCardId);

    //         const fileData = [...wordsStore, ...wordsItem];

    //         fs.writeFileSync(this.fullpath, JSON.stringify(fileData));
    //     } catch (err) {
    //         console.error('An error occurred while reading the file:', err);
    //     }
    // }

    // bulkUpdateWords(updatedWords: WordStoreItem[]) {
    //     try {
    //         const updatedWordsMappedById = updatedWords.reduce((mappedWords, updatedWord) => {
    //             mappedWords[updatedWord.id] = updatedWord;

    //             return mappedWords;
    //         } ,{} as Record<string, WordStoreItem>);

    //         const wordsStore = this.getWordsStore()
            
    //         const updatedStore = wordsStore.map((word) => {
    //             if (updatedWordsMappedById[word.id]) {
    //                 return updatedWordsMappedById[word.id];
    //             }
                
    //             return word;
    //         })

    //         fs.writeFileSync(this.fullpath, JSON.stringify(updatedStore), 'utf8');
    //     } catch (err) {
    //         console.error('An error occurred while reading the file:', err);
    //     }
    // }

    // removeCardById(cardId: number) {
    //     try {
    //         const wordsStore = this.getWordsStore();
    //         const updatedWordsStore = wordsStore.filter((word => word.cardId !== cardId));

    //         fs.writeFileSync(this.fullpath, JSON.stringify(updatedWordsStore));
    //     } catch (err) {
    //         throw err;
    //     }
    // }

    
    getCards() {
        const cardNameTeplate = 'Card #';

        const cardIds = this.wordsStore.reduce((res, { cardId }) => {
            if (!res[cardId]) {
                res[cardId] = { id: cardId, name: `${cardNameTeplate}${cardId}` };
            }

            return res;
        }, {} as Record<string, any>);

        return Object.values(cardIds);
    }

    getWordsByCardId(cardId: number): WordStoreItem[]  {
        try {
            const wordsStore = this.wordsStore;
            
            return wordsStore.filter((word) => word.cardId === cardId);

        } catch (err) {
            throw(err)
        }
    }

    updateWord(updatedWord: WordStoreItem) {
        try {
            const updatedStore = this.wordsStore.map((word) => {
                if (updatedWord.id === word.id) {
                    return updatedWord;
                }
                
                return word;
            })

            this.wordsStore = updatedStore;

            return updatedWord;
        } catch (err) {
            console.error('An error occurred while reading the file:', err);
        }
    }

    addWord(word: WordStoreItem) {
        try {
            const newWord = {
                ...word,
                id: uniqid(),
                correctAnwers: 0,
            
            };
            const updatedStore = [...this.wordsStore, newWord];

            this.wordsStore = updatedStore;

            return newWord;
        } catch (err) {
            console.error('An error occurred while reading the file:', err);
        }
    }

    removeWord(wordId: string) {
        const updatedStore = this.wordsStore.filter(({ id }) => id !== wordId);

        this.wordsStore = updatedStore;

        return this.wordsStore;
    }
}

const wordStore = new WordList('./', 'words.json');

export { wordStore };