export const words = [
    {"word":"earlier","tranlations":["ранее","раньше","назад"],"date":"27/06/2023"},
    {"word":"teeth","tranlations":["зубы","зубов","зубами"],"date":"27/06/2023"},
    {"word":"along with","tranlations":["вместе с","наряду с","а также"],"date":"27/06/2023"},
    {"word":"derived from","tranlations":["походить від","отримані з","походять від"],"date":"27/06/2023"},
    {"word":"participle","tranlations":["дієприкметник","причастя"],"date":"27/06/2023"},
    
    {"word":"private appointments","tranlations":["частных встречах"],"date":"27/06/2023"},
    {"word":"surgery","tranlations":["операция","хирургия","хирургическое вмешательство"],"date":"27/06/2023"},
    {"word":"prescription","tranlations":["рецепт","назначение","предписание"],"date":"27/06/2023"},
    {"word":"make an appointment with the doctor","tranlations":["записывать к врачу"],"date":"27/06/2023"},
    {"word":"прикус","tranlations":["bite","occlusion","underbite"],"date":"27/06/2023"},
    
    {"word":"enquiry","tranlations":["запрос","расследование","исследование"],"date":"27/06/2023"},
    {"word":"enhanced","tranlations":["посилений","розширений","підвищений"],"date":"27/06/2023"},
    {"word":"make an appointment","tranlations":["записаться","назначать встречу","договариваться о встрече"],"date":"27/06/2023"},
    {"word":"enhanced","tranlations":["расширенный","усиленный","усовершенствованный"],"date":"27/06/2023"},
    {"word":"render","tranlations":["надавати","надати","передати"],"date":"26/06/2023"},
    
    {"word":"might","tranlations":["може","можуть","можете"],"date":"26/06/2023"},
    {"word":"adjust","tranlations":["регулювати","налаштувати","відрегулювати"],"date":"26/06/2023"},
    {"word":"unintentionally","tranlations":["ненавмисно","випадково","мимоволі"],"date":"26/06/2023"},
    {"word":"significantly","tranlations":["значно","істотно","суттєво"],"date":"26/06/2023"},
    {"word":"on-demand","tranlations":["на вимогу","за запитом","за вимогою"],"date":"26/06/2023"},
    
    {"word":"persists","tranlations":["зберігається","триває","залишається"],"date":"26/06/2023"},
    {"word":"aims","tranlations":["спрямований","прагне","цілі"],"date":"26/06/2023"},
    {"word":"fascinated","tranlations":["зачарований","захоплений","вражений"],"date":"26/06/2023"}
]