import { words } from './words';
import { wordStore } from './WordController';

export const getCardById = (id: string) => {
    const numberId = Number(id);
    return wordStore.getWordsByCardId(numberId);
};

export const getCardIds = () => {
    return wordStore.getCards().map(({ id }) => ({
        params: {
            id: `${id}`
        }
    }))
};