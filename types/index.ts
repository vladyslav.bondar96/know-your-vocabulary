export interface Word { 
    id: string;
    word: string;
    tranlations: string[];
    date: string;
    cardId: number;
    correctAnwers: number;
    note?: string;
};