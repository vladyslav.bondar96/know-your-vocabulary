
export const shuffleArray = (array: Array<any>, itemModifier?: (item: any) => any ): Array<any> => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = itemModifier ? itemModifier(array[i]) : array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    return [...array];
}